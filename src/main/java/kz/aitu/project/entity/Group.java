package kz.aitu.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="groupp")
public class Group {
        @Id
        @Column(name="groupid")
        private int groupId;

        @Column(name="groupname")
        private String groupName;

    @Override
    public String toString() {
        return "Group id: " + groupId + "   Student id: " + groupName;
    }

}
