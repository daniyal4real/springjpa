package kz.aitu.project.repository;

import kz.aitu.project.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    @Query(value = "SELECT * FROM groupp WHERE groupid = ?", nativeQuery = true)
    List<Group> findGroupById(int id);

    @Query(value = "SELECT * FROM groupp ORDER BY groupid", nativeQuery = true)
    List<Group> ownFindAll();
}
