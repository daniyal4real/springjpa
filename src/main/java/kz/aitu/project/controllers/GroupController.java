package kz.aitu.project.controllers;

import kz.aitu.project.entity.Group;
import kz.aitu.project.repository.GroupRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class GroupController {
    private GroupRepository groupRepository;

    public GroupController(GroupRepository groupRepository){
        this.groupRepository = groupRepository;
    }

    @GetMapping("/groups")
    public ResponseEntity<?> getAllGroups(){
        return ResponseEntity.ok(groupRepository.findAll());
    }

    @GetMapping("/groups/{id}")
    public ResponseEntity<List<Group>> getGroupById(@PathVariable int id){
        return ResponseEntity.ok(groupRepository.findGroupById(id));
    }

    @GetMapping("/groups/all")
    public List<Group> getAll(){
        return groupRepository.ownFindAll();
    }
}
